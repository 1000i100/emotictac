# FR : Confidentialité, vie privée & traitement des données dans EmoTicTac

Le site ne collecte rien, pas de cookie, de télémétrie de mouchards, de librairie indiscrète... Rien.
Le serveur qui héberge doit bien avoir des logs pour respecter ses obligations légales. Ces derniers contiennent : votre IP, la page que vous avez consulté, la date et l'heure de consultation.
La version application ne collecte rien de plus, mais si vous la téléchargez via un store, le store lui conserve au moins le nombre de téléchargement et peutêtre d'autres choses. Vous pouvez consulter les règles de confidentialité / privacy du store si vous le souhaitez, ça n'est pas de mon ressort.

Voila c'est tout. C'est l'avantage d'un outil libre qui ne cherche pas à vous soutirer quoi que ce soit.

## EN : Privacy

Nothing is collected, it's that simple !
