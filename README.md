# EmoTicTac / symbolic-emotional-state

Rendez visible votre état émotionnel en un clic.

Acculturez vos collègues à tenir compte de votre état émotionnel en le rendant visible pour celleux qui ne savent ou ne pensent pas à lire sur les visages !

## [Tester en ligne](https://1000i100.frama.io/symbolic-emotional-state/)
Possible aussi à [plusieurs sur le même appareil](https://1000i100.frama.io/symbolic-emotional-state/multi.html).


## Un outil issu d'expérimentations low-tech d'outils de facilitation

Après plusieurs années à utiliser des signaux gestuels pour communiquer en groupe
sans interrompre la personne qui a la parole, j'ai identifié quelques limites :
- il me semble difficile socialement de garder visible un signal gestuel durablement s'il n'y a pas de réaction chez les autres.
- il devient même difficile pour certain·e·s d'oser en utiliser sans les voir préalablement utilisés et pris en compte efficacement par les autres.
En 2019, j'ai voulu expérimenter des alternatives ou des outils complémentaires.  
J'ai conçu et testé plusieurs outils pour essayer de dépasser ces limites.
Je les considère à mi-chemin entre des outils d'éducation populaire et des outils d'aide à la co-facilitation.
Mon objectif avec ces outils est d'aider les participant·e·s à être vigilant·e·s à leur état de consentement et à celui des autres.

Pour cela les **cartons du consentement** les invitent à prendre conscience de leur état émotionnel
et à le rendre simple d'accès pour les autres grâce à la codification colorée des cartons.


### Les cartons du consentement

Comment utiliser des cartons colorés pour se placer dans le spectre du consentement :

- Chaque participant·e a une pile devant soi avec un carton de chaque couleur dedans.
  Le carton du dessus (le seul visible) indique où en est le·la participant·e.
- Chaque participant·e est responsable de rendre visible son état de consentement en plaçant le bon carton au-dessus.
  Iel facilite ainsi la prise en compte de son état par les autres grâce au côté explicite et visible apporté par l'outil.

Entre simplicité et finesse, les états de consentement retenus sont les suivant :

| Couleur | Etat            | Interprétation suggérée     |
| ------- | --------------- | --------------------------- |
| Vert    | Enthousiaste !  | Cool ! Dis-m’en plus !      |
| Jaune   | Neutre Ok       | Vas à l’essentiel           |
| Orange  | Je fatigue      | On fait tourner la parole ? |
| Rouge   | J’en peux plus. | STOP ! L'écoute individuelle de quelqu'un qui a ma confiance : peut-être, sans l’imposer. |

### Les sabliers du consentement

En dépassant la limite de la communiation gestuelle qui est difficile à maintenir dans le temps
quand elle n'est pas prise en compte immédiatement, j'ai introduit avec les cartons un autre travers :
Si un·e participant·e ne change pas le carton actif alors que son état change, le carton se met à induire en erreur.

C'est de ce constat qu'est né l'expérimentation de sablier du consentement.

**L'objectif : Prendre soin des personnes en difficulté, même si elles ne sont pas capables de l'exprimer.**

Comment ?

Chacun met à s'écouler son sablier visible devant soi.
1. Si quelque chose ne va pas, changez-le ou parlez en sans attendre. Si vous n'en avez pas l'énergie, c'est ok, grâce au sablier, votre entourage va s'en apercevoir et prendre soin de vous.
2. Si votre **sablier** à **fini** de s'écouler, **avez-vous envie de continuer ?** Oui -> retourner votre sablier. Non -> Etape 1.
3. Si vous voyez que **le sablier de quelqu'un·e reste vide** plusieurs secondes, demandez-lui discrètement : 
   Qu'est ce qui se passe pour toi ? **Où en es-tu ?** Veux-tu qu'on aille en parler plus au calme ?

Les bénéfices de l'usage des sabliers du consentement :
- apprendre à se questionner régulièrement sur notre consentement ou notre enthousiasme à continuer à vivre ce qui a lieu.
- se sentir légitime à s'enquérir de l'état des autres.
- apprendre à être vigilant à l'état des autres.
- gouter à la sécurité de voir les autres prendre soin de nous quand on oublie de retourner son sablier,
  et du coup se sentir plus en sécurité pour aborder des sujets qui peuvent être remuant émotionnellement.
- conserver ces apprentissages cette conscience de soi et des autres, dans de futures conversations ou activités sans sablier.

Les défauts identifiés des sabliers du consentement :
- il faut un sablier par participant·e.
- manque de souplesse : un sablier de 3 minutes, ça peut à la fois être très long quand ça va mal,
  et très court quand se poser la question de le retourner nous détourne de ce qui nous intéresse toutes les 3 minutes.
- manque de nuances : le sablier est binaire, il s'écoule ou non, les cartons permettent plus de finesse.


C'est pour contourner ces défauts en gardant le meilleur des deux outils que j'ai conçus **EmoTicTac**.
- Davantage de nuances avec un spectre d'émoticônes et de couleurs.
- Davantage de souplesse en pouvant ajuster son état à tout moment.
- Davantage de souplesse aussi avec le rythme de présomption de désenchantement ajustable.
- Utilisable à l'improviste, sans matériel personnel, si suffisamment de participant·e·s ont des smartphones.


## Crédit et licence
Le projet est sous licence AGPL-3.0

Mike Koenig à produit le fichier [bleep.mp3](http://soundbible.com/1252-Bleep.html) et [gong.mp3](http://soundbible.com/1531-Temple-Bell.html)
