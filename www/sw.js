var CACHE = 'cache-only';

self.addEventListener('install', function (evt) {
	console.log('The service worker is being installed.');
	evt.waitUntil(precache());
});


self.addEventListener('fetch', function (evt) {
	console.log('The service worker is serving the asset.');
	evt.respondWith(fromCache(evt.request));
},{ redirect: 'follow' });

function precache() {
	return caches.open(CACHE).then(function (cache) {
		return cache.addAll([
			'./',
			'./index.html',
			'./multi.html',
			'./bleep.mp3',
			'./gong.mp3',
			'./manifest.json',
			'icone/android-icon-36x36.png',
			'icone/android-icon-48x48.png',
			'icone/android-icon-72x72.png',
			'icone/android-icon-96x96.png',
			'icone/android-icon-144x144.png',
			'icone/android-icon-192x192.png',
			'icone/apple-icon.png',
			'icone/apple-icon-57x57.png',
			'icone/apple-icon-60x60.png',
			'icone/apple-icon-72x72.png',
			'icone/apple-icon-76x76.png',
			'icone/apple-icon-114x114.png',
			'icone/apple-icon-120x120.png',
			'icone/apple-icon-144x144.png',
			'icone/apple-icon-152x152.png',
			'icone/apple-icon-180x180.png',
			'icone/apple-icon-precomposed.png',
			'icone/favicon.ico',
			'icone/favicon-16x16.png',
			'icone/favicon-32x32.png',
			'icone/favicon-96x96.png',
			'icone/ms-icon-70x70.png',
			'icone/ms-icon-144x144.png',
			'icone/ms-icon-150x150.png',
			'icone/ms-icon-310x310.png',
			'icone/SES-HD.png'
		]);
	});
}


function fromCache(request) {
	return caches.open(CACHE).then(function (cache) {
		return cache.match(request).then(function (matching) {
			return matching || Promise.reject('no-match');
		});
	});
}

